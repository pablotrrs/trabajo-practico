#!/bin/bash
#------------------------------------------------------
#		     PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/ setab: color de fondo
    red=`tput setaf 1`;
    bg_green=`tput setab 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
#		    VARIABLES GLOBALES
#------------------------------------------------------
directorio=$(pwd);
#------------------------------------------------------
#		       DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "S  U  P  E  R     M  E  N U                       ";
    
    echo -e "\t\t\t        proyecto actual";
    echo -e "\t\t  $directorio";
    
    echo -e "\t\t";
    echo -e "\t\t\t\t   opciones";
    echo "";
    echo -e "\t\t\t a. Ver contenido de carpeta";
    echo -e "\t\t\t b. Operar en Gitlab";
    echo -e "\t\t\t c. Crear 100 carpetas";
    echo -e "\t\t\t d. Eliminar las 100 carpetas";
    echo -e "\t\t\t e. Operaciones matematicas";
    echo -e "\t\t\t f. La barberia";
    echo -e "\t\t\t g. Opera de Gioachino Rossini";
    echo -e "\t\t\t q. Salir";
    echo "";
    echo -e "\t\t     escriba la opcion y presione ENTER";
}

#------------------------------------------------------
#		  FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #para arreglar la hora que estaba adelantada
    hora="`date +"%H" `"
    espacios="                                          "
    espacios2="                                   "
    guiones="------------------------------------------------------------------------------"
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario esta ejecutando
    echo -e "${bg_green} ${blue}`date +"%d-%m-%Y $(( ${hora#0} -3 )):%M:%S"`$espacios  USUARIO:$USER ${reset}";
    echo -e "${bg_green}$espacios$espacios2   ";
    #Se agregan colores a encabezado
    echo -e "${bg_green} ${blue}$guiones \t${reset}";
    echo -e "${bg_green}${blue}                           $1$2   ";
    echo -e "${bg_green} ${blue}$guiones \t${reset}";
    echo -e "${bg_green}$espacios  $espacios2 ${reset} ";
}

esperar () {
    echo "";
    echo -e "\t\t   presione ENTER para volver al menu principal";
    read ENTER;
}

malaEleccion () {
    echo -e "Seleccion Invalida ..." ;
}

decidir () {
    #$1 es lo que se le pasa en la funcion de la opcion
    echo $1;
    while true;
     do
        echo "desea ejecutar? (s/n)";
            read respuesta;
	    echo "";
            case $respuesta in
                [Nn]* ) break;;
		#eval ejecuta el comando pasado como parametro
                [Ss]* ) eval $1
                break;;
                * ) echo "Por favor tipear S/s o N/n.";;
            esac
    done
}

#------------------------------------------------------
#		   FUNCIONES del MENU
#------------------------------------------------------
a_funcion () {
	imprimir_encabezado "Opcion a. Ver contenido del proyecto              ";
	echo "listar archivos?"
	decidir "ls | sort"
	echo -e "\ndesea ver archivos ocultos?"
	decidir "ls -a | sort"
}

b_funcion () {
	imprimir_encabezado "Opcion b. Operar en Gitlab                        ";
	echo -e "\t\t\t     Que es lo que desea hacer?\n\t\t\t     a. Actualizar el repositorio\n\t\t\t     b. Chequear el estado de los archivos\n\t\t\t     c. Realizar 'git add'/'git commit' \n\t\t\t     para todos los archivos pendientes\n\t\t\t     d. Realizar 'git push'\n\t\t\t     e. Subir archivo/carpeta\n\t\t\t     f. Crear archivo y subir\n\t\t\t     g. Crear carpeta y subir\n\t\t\t     h. Borrar archivo/carpeta\n\t\t\t     i. Abrir Gitlab\n\t\t\t     q. Salir"
	read desea
	cd $directorio
	while true
	do
	echo "";
	case $desea in 
		a|A) git pull;;
		b|B) git status ;;
		c|C) cometer;;
		d|D) echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena   " && git push origin master;;
		e|E) subir;;
		f|F) crear_archivo;;
		g|G) crear_carpeta;;
		h|H) borrar;;
		i|I) xdg-open "https://gitlab.com/pablo0210/trabajo-practico";;
		q|Q) break;;
		*) malaEleccion;;
	esac
	break
	done
}

subir () {
	read -p "Por favor ingrese la ruta donde se encuentra su archivo: " ruta
	cd $ruta
	read -p "Por favor ingrese el nombre de su archivo/carpeta: " archivo
	find | grep -q "$archivo" #busca el nombre al archivo a traves de las carpetas
	existe=$?
	if [ $existe -eq 0 ]
	then
		cp -r $archivo $directorio
		cd $directorio
		git add $archivo
	        git commit -q -m "Creado a traves del supermenu"
	        echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena   "
	        git push -q origin master 
		resultado=$?
	        case $resultado in
        	        0) echo -e "\n\t\t    su archivo ya ha sido modificado en Gitlab";;
                	*) echo -e "\n\t\t    su archivo no ha sido modificado en Gitlab";;
	        esac
	else
		echo -e "\n$archivo no existe"
	fi
	cd $directorio
}

cometer () {
	read -p "Ingrese un mensaje para el commit: " com
	git add --all && git commit -a -q -m "$com"
	echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena   "
        git push -q origin master
	resultado=$? 
        case $resultado in
                0) echo -e "\n\t\t  sus archivos ya han sido modificados en Gitlab";;
                *) echo -e "\n\t\t  sus archivos no han sido modificados en Gitlab";;
        esac

}

crear_archivo () {
	read -p "Por favor ingrese el nombre de su archivo: " archivo
	nano $archivo
        echo -e "\t\t\t\tCREADO A TRAVES DEL SUPERMENU" >> $archivo
	echo -e "\nRevisar el contenido de $archivo ?"
	decidir "cat $archivo"
	git add $archivo
	echo -e ""
	read -p "Ingrese un mensaje para el commit: " com
	git commit -q -m "$com"
	echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena   "
	git push -q origin master 
	#para detectar si hubo algun error en la subida
	resultado=$? 
	case $resultado in
                0) echo -e "\n\t\t    su archivo ya ha sido modificado en Gitlab";;
               	*) echo -e "\n\t\t    su archivo no ha sido modificado en Gitlab";;
	esac
}

crear_carpeta () {
	read -p "Por favor ingrese el nombre de su carpeta: " carpeta
	mkdir $carpeta
	echo vacio > $carpeta/vacio.txt
        git add $carpeta/
	read -p "Ingrese un mensaje para el commit: " com
        git commit -q -m "$com"
	echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena"
	git push -q origin master
	resultado=$?
	case $resultado in
		0) echo -e "\n\t\t    su carpeta ya ha sido modificada en Gitlab";;
		*) echo -e "\n\t\t    su carpeta no ha sido modificada en Gitlab";;
	esac
}

borrar () {
	read -p "Por favor ingrese el nombre de su archivo/carpeta: " archivo
	if [ -f $archivo ]
        then
		rm $archivo
                git add $archivo
        elif [ -r $archivo ]
        then
                rm -r $archivo
                git add $archivo
        fi

	git commit -q -m "Eliminado a traves del supermenu"
	echo "Ahora debe ingresar primero su nombre de usuario y luego la contrasena   "
	git push -q origin master
	resultado=$?
	case $resultado in
                0) echo -e "\n\t\t     su archivo ya ha sido eliminado de Gitlab";;
                *) echo -e "\n\t\t     su archivo no ha sido eliminado de Gitlab";;
        esac
}

c_funcion () { 
	imprimir_encabezado "Opcion c. Crear 100 carpetas                      ";
	read -p "Indique la ruta donde deben ser creadas las carpetas: " ruta
	cd $ruta
	for ((i=0;i<100;i++))
	do
	mkdir -p $ruta/CarpetaMadre/$i
	chmod u+rw-x,g+r-wx,o-rwx $ruta/CarpetaMadre/$i
	done	
	echo -e "\t   han sido creadas las carpetas nombradas desde el 0 hasta el 99"
	echo -e "\t\t\t\t¿desea ver sus permisos?"
	cd $ruta/CarpetaMadre
	decidir "ls -l | sort"
	cd $directorio
}

d_funcion () {
	imprimir_encabezado "Opcion d. Eliminar las 100 carpetas               ";
	read -p "Indique la ruta de donde deben ser eliminadas las carpetas: " b
        cd $b
	if [ -r "$b/CarpetaMadre" ]
	then
		rm -r  $b/CarpetaMadre
		echo -e "\t\t    las carpetas han sido eliminadas con exito"
	else 
                echo -e "\t\t  las carpetas no existen, primero debe crearlas"
	fi
	cd $directorio
}

e_funcion () {
	imprimir_encabezado "Opcion e. Operaciones matematicas                 ";        
	echo ""
	decidir "./calculosYES"
}

f_funcion () {
        imprimir_encabezado "Opcion e. La barberia                             ";        
	echo -e "\nIngrese la cantidad de asientos de los que dispone la barberia\nrecuerde que van a pasar 10 clientes: ";
	read clientes;
	echo "";
	decidir	"./barberia $clientes";
}

g_funcion () {
        imprimir_encabezado "Opcion e. Opera de Gioachino Rossini              ";
        echo -e "\nIngrese la cantidad de veces que Rossini debe efectuar su canto:\n";
        read canto;
        echo "";
        decidir "./figaro $canto";
}

#------------------------------------------------------
#		    LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
	f|F) f_funcion;;
	g|G) g_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
